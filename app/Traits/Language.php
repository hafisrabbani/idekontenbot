<?php

namespace App\Traits;

trait Language
{

    public function languageList()
    {
        $languages =
            [
                "af-ZA"     => localize('Afrikaans (South Africa)'),
                "ar-XA"     => localize('Arabic'),
                "eu-ES"     => localize('Basque (Spain)'),
                "bn-IN"     => localize('Bengali (India)'),
                "bg-BG"     => localize('Bulgarian (Bulgaria)'),
                "ca-ES"     => localize('Catalan (Spain) '),
                "yue-HK"    => localize('Chinese (Hong Kong)'),
                "cs-CZ"     => localize('Czech (Czech Republic)'),
                "da-DK"     => localize('Danish (Denmark)'),
                "nl-BE"     => localize('Dutch (Belgium)'),
                "nl-NL"     => localize('Dutch (Netherlands)'),
                "en-AU"     => localize('English (Australia)'),
                "en-IN"     => localize('English (India)'),
                "en-GB"     => localize('English (UK)'),
                "en-US"     => localize('English (US)'),
                "fil-PH"    => localize('Filipino (Philippines)'),
                "fi-FI"     => localize('Finnish (Finland)'),
                "fr-CA"     => localize('French (Canada)'),
                "fr-FR"     => localize('French (France)'),
                "gl-ES"     => localize('Galician (Spain)'),
                "de-DE"     => localize('German (Germany)'),
                "el-GR"     => localize('Greek (Greece)'),
                "gu-IN"     => localize('Gujarati (India)'),
                "he-IL"     => localize('Hebrew (Israel)'),
                "hi-IN"     => localize('Hindi (India)'),
                "hu-HU"     => localize('Hungarian (Hungary)'),
                "is-IS"     => localize('Icelandic (Iceland)'),
                "id-ID"     => localize('Indonesian (Indonesia)'),
                "it-IT"     => localize('Italian (Italy)'),
                "ja-JP"     => localize('Japanese (Japan)'),
                "kn-IN"     => localize('Kannada (India)'),
                "ko-KR"     => localize('Korean (South Korea)'),
                "lv-LV"     => localize('Latvian (Latvia)'),
                "ms-MY"     => localize('Malay (Malaysia)'),
                "ml-IN"     => localize('Malayalam (India)'),
                "cmn-CN"    => localize('Mandarin Chinese'),
                "cmn-TW"    => localize('Mandarin Chinese (T)'),
                "mr-IN"     => localize('Marathi (India)'),
                "nb-NO"     => localize('Norwegian (Norway)'),
                "pl-PL"     => localize('Polish (Poland)'),
                "pt-BR"     => localize('Portuguese (Brazil)'),
                "pt-PT"     => localize('Portuguese (Portugal)'),
                "pa-IN"     => localize('Punjabi (India)'),
                "ro-RO"     => localize('Romanian (Romania)'),
                "ru-RU"     => localize('Russian (Russia)'),
                "sr-RS"     => localize('Serbian (Cyrillic)'),
                "sk-SK"     => localize('Slovak (Slovakia)'),
                "es-ES"     => localize('Spanish (Spain)'),
                "es-US"     => localize('Spanish (US)'),
                "sv-SE"     => localize('Swedish (Sweden)'),
                "ta-IN"     => localize('Tamil (India)'),
                "te-IN"     => localize('Telugu (India)'),
                "th-TH"     => localize('Thai (Thailand)'),
                "tr-TR"     => localize('Turkish (Turkey)'),
                "uk-UA"     => localize('Ukrainian (Ukraine)'),
                "vi-VN"     => localize('Vietnamese (Vietnam)')
            ];
        return $languages;
    }
    public function languageVoicesData()
    {


        $voicesData = [
            "af-ZA" => [
                [
                    'value' => "af-ZA-Standard-A",
                    'label' => "Standart - FEMALE"
                ]
            ],
            "ar-XA" => [
                [
                    'value' => "ar-XA-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ar-XA-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ar-XA-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ar-XA-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ar-XA-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ar-XA-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ar-XA-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ar-XA-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "eu-ES" => [[
                'value' => "eu-ES-Standard-A",
                'label' => "Standart - FEMALE"
            ]],
            "bn-IN" => [
                [
                    'value' => "bn-IN-Standard-A",
                    'label' => "Standart - FEMALE"
                ],
                [
                    'value' => "bn-IN-Standard-B",
                    'label' => "Standart - MALE"
                ],
                [
                    'value' => "bn-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "bn-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "bg-BG" => [[
                'value' => "bg-BG-Standard-A",
                'label' => "Standart - FEMALE"
            ]],
            "ca-ES" => [[
                'value' => "ca-ES-Standard-A",
                'label' => "Standart - FEMALE"
            ]],
            "yue-HK" => [
                [
                    'value' => "yue-HK-Standard-A",
                    'label' => "Standart - FEMALE"
                ],
                [
                    'value' => "yue-HK-Standard-B",
                    'label' => "Standart - MALE"
                ],
                [
                    'value' => "yue-HK-Standard-C",
                    'label' => "Standart - FEMALE"
                ],
                [
                    'value' => "yue-HK-Standard-D",
                    'label' => "Standart - MALE"
                ]
            ],
            "cs-CZ" => [
                [
                    'value' => "cs-CZ-Standard-A",
                    'label' => "Standart - FEMALE"
                ],
                [
                    'value' => "cs-CZ-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "da-DK" => [

                [
                    'value' => "da-DK-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "da-DK-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "da-DK-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "da-DK-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "da-DK-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "da-DK-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "da-DK-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "da-DK-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "da-DK-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "da-DK-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "nl-BE" => [
                [
                    'value' => "nl-BE-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nl-BE-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "nl-BE-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "nl-BE-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "nl-NL" => [
                [
                    'value' => "nl-NL-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nl-NL-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "nl-NL-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "nl-NL-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nl-NL-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nl-NL-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "nl-NL-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "nl-NL-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "nl-NL-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "nl-NL-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "en-AU" => [

                [
                    'value' => "en-AU-News-E",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-AU-News-F",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-AU-News-G",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-AU-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-AU-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-AU-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-AU-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-AU-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-AU-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-AU-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-AU-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-AU-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "en-IN" => [
                [
                    'value' => "en-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-IN-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-IN-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-IN-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-IN-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "en-GB" => [

                [
                    'value' => "en-GB-News-G",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-News-H",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-News-I",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-News-J",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-News-K",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-GB-News-L",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-GB-News-M",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-GB-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-GB-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-GB-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-GB-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-GB-Standard-F",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-GB-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-GB-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-GB-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-GB-Wavenet-F",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "en-US" => [

                [
                    'value' => "en-US-News-K",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-News-L",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-News-M",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-News-N",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Standard-A",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-US-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-US-Standard-F",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-US-Standard-G",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-US-Standard-H",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "en-US-Standard-I",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Standard-J",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "en-US-Studio-M",
                    'label' => "Studio - MALE"
                ],
                [
                    'value' => "en-US-Studio-O",
                    'label' => "Studio - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-A",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-F",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-G",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-H",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "en-US-Wavenet-I",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "en-US-Wavenet-J",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "fil-PH" => [
                [
                    'value' => "fil-PH-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fil-PH-Standard-B",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fil-PH-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fil-PH-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fil-PH-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fil-PH-Wavenet-B",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fil-PH-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "fil-PH-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],

            ],
            "fi-FI" => [
                [
                    'value' => "fi-FI-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fi-FI-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "fr-CA" => [

                [
                    'value' => "fr-CA-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fr-CA-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fr-CA-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fr-CA-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fr-CA-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fr-CA-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "fr-CA-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fr-CA-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "fr-FR" => [

                [
                    'value' => "fr-FR-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fr-FR-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fr-FR-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fr-FR-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fr-FR-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "fr-FR-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "fr-FR-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fr-FR-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "fr-FR-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "fr-FR-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "fr-FR-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "gl-ES" => [[
                'value' => "gl-ES-Standard-A",
                'label' => "Standard - FEMALE"
            ]],
            "de-DE" => [

                [
                    'value' => "de-DE-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "de-DE-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "de-DE-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "de-DE-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "de-DE-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "de-DE-Standard-E",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "de-DE-Standard-F",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "de-DE-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "de-DE-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "de-DE-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "de-DE-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "de-DE-Wavenet-E",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "de-DE-Wavenet-F",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "el-GR" => [
                [
                    'value' => "el-GR-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "el-GR-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "gu-IN" => [
                [
                    'value' => "gu-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "gu-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "gu-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "gu-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "he-IL" => [
                [
                    'value' => "he-IL-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "he-IL-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "he-IL-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "he-IL-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "he-IL-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "he-IL-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "he-IL-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "he-IL-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "hi-IN" => [

                [
                    'value' => "hi-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "hi-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "hi-IN-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "hi-IN-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "hi-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "hi-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "hi-IN-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "hi-IN-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "hu-HU" => [
                [
                    'value' => "hu-HU-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "hu-HU-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "is-IS" => [[
                'value' => "is-IS-Standard-A",
                'label' => "Standard - FEMALE"
            ]],
            "id-ID" => [
                [
                    'value' => "id-ID-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "id-ID-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "id-ID-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "id-ID-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "id-ID-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "id-ID-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "id-ID-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "id-ID-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "it-IT" => [

                [
                    'value' => "it-IT-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "it-IT-Standard-B",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "it-IT-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "it-IT-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "it-IT-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "it-IT-Wavenet-B",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "it-IT-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "it-IT-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "ja-JP" => [

                [
                    'value' => "ja-JP-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ja-JP-Standard-B",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ja-JP-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ja-JP-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ja-JP-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ja-JP-Wavenet-B",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ja-JP-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ja-JP-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "kn-IN" => [
                [
                    'value' => "kn-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "kn-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "kn-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "kn-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "ko-KR" => [

                [
                    'value' => "ko-KR-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ko-KR-Standard-B",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ko-KR-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ko-KR-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ko-KR-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ko-KR-Wavenet-B",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ko-KR-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ko-KR-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "lv-LV" => [[
                'value' => "lv-LV-Standard-A",
                'label' => "Standard - MALE"
            ]],
            "lv-LT" => [[
                'value' => "lv-LT-Standard-A",
                'label' => "Standard - MALE"
            ]],
            "ms-MY" => [
                [
                    'value' => "ms-MY-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ms-MY-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ms-MY-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ms-MY-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ms-MY-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ms-MY-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ms-MY-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ms-MY-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "ml-IN" => [
                [
                    'value' => "ml-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ml-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ml-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ml-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ml-IN-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ml-IN-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "cmn-CN" => [
                [
                    'value' => "cmn-CN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "cmn-CN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "cmn-CN-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "cmn-CN-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "cmn-CN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "cmn-CN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "cmn-CN-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "cmn-CN-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "cmn-TW" => [
                [
                    'value' => "cmn-TW-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "cmn-TW-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "cmn-TW-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "cmn-TW-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "cmn-TW-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "cmn-TW-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "mr-IN" => [
                [
                    'value' => "mr-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "mr-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "mr-IN-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "mr-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "mr-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "mr-IN-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "nb-NO" => [
                [
                    'value' => "nb-NO-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nb-NO-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "nb-NO-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nb-NO-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "nb-NO-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "nb-NO-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "nb-NO-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "nb-NO-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "nb-NO-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "nb-NO-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "pl-PL" => [
                [
                    'value' => "pl-PL-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pl-PL-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pl-PL-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pl-PL-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pl-PL-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pl-PL-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pl-PL-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pl-PL-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pl-PL-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pl-PL-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "pt-BR" => [

                [
                    'value' => "pt-BR-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pt-BR-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pt-BR-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pt-BR-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pt-BR-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pt-BR-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "pt-PT" => [
                [
                    'value' => "pt-PT-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pt-PT-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pt-PT-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pt-PT-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pt-PT-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pt-PT-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pt-PT-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pt-PT-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "pa-IN" => [
                [
                    'value' => "pa-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pa-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pa-IN-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "pa-IN-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "pa-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pa-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "pa-IN-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "pa-IN-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "ro-RO" => [
                [
                    'value' => "ro-RO-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ro-RO-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "ru-RU" => [
                [
                    'value' => "ru-RU-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ru-RU-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ru-RU-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ru-RU-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ru-RU-Standard-E",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ru-RU-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ru-RU-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ru-RU-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ru-RU-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ru-RU-Wavenet-E",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "sr-RS" => [[
                'value' => "sr-RS-Standard-A",
                'label' => "Standard - FEMALE"
            ]],
            "sk-SK" => [
                [
                    'value' => "sk-SK-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "sk-SK-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "es-ES" => [

                [
                    'value' => "es-ES-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "es-ES-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "es-ES-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "es-ES-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "es-ES-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "es-ES-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "es-ES-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "es-ES-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "es-US" => [

                [
                    'value' => "es-US-News-D",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "es-US-News-E",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "es-US-News-F",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "es-US-News-G",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "es-US-Polyglot-1",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "es-US-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "es-US-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "es-US-Standard-C",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "es-US-Studio-B",
                    'label' => "Studio - MALE"
                ],
                [
                    'value' => "es-US-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "es-US-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "es-US-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "sv-SE" => [
                [
                    'value' => "sv-SE-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "sv-SE-Standard-B",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "sv-SE-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "sv-SE-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "sv-SE-Standard-E",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "sv-SE-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "sv-SE-Wavenet-B",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "sv-SE-Wavenet-C",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "sv-SE-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "sv-SE-Wavenet-E",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "ta-IN" => [
                [
                    'value' => "ta-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ta-IN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ta-IN-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "ta-IN-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "ta-IN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ta-IN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "ta-IN-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "ta-IN-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "te-IN" => [
                [
                    'value' => "-IN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "-IN-Standard-B",
                    'label' => "Standard - MALE"
                ]
            ],
            "th-TH" => [

                [
                    'value' => "th-TH-Standard-A",
                    'label' => "Standard - FEMALE"
                ]
            ],
            "tr-TR" => [
                [
                    'value' => "tr-TR-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "tr-TR-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "tr-TR-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "tr-TR-Standard-D",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "tr-TR-Standard-E",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "tr-TR-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "tr-TR-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "tr-TR-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "tr-TR-Wavenet-D",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "tr-TR-Wavenet-E",
                    'label' => "WaveNet - MALE"
                ]
            ],
            "uk-UA" => [
                [
                    'value' => "uk-UA-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "uk-UA-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ]
            ],
            "vi-VN" => [

                [
                    'value' => "vi-VN-Standard-A",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "vi-VN-Standard-B",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "vi-VN-Standard-C",
                    'label' => "Standard - FEMALE"
                ],
                [
                    'value' => "vi-VN-Standard-D",
                    'label' => "Standard - MALE"
                ],
                [
                    'value' => "vi-VN-Wavenet-A",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "vi-VN-Wavenet-B",
                    'label' => "WaveNet - MALE"
                ],
                [
                    'value' => "vi-VN-Wavenet-C",
                    'label' => "WaveNet - FEMALE"
                ],
                [
                    'value' => "vi-VN-Wavenet-D",
                    'label' => "WaveNet - MALE"
                ]
            ]
        ];
        return $voicesData;
        if (voiceOverEnable() == 'azure') {
            $dk_naural = ['da-DK' => [
                [
                    'value' => "da-DK-Neural2-D",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "da-DK-Neural2-F",
                    'label' => "Neural2 - MALE"
                ],
            ]];
            $voicesData = array_merge($voicesData, $dk_naural);

            $en_GB_nural = ['en-GB' => [
                [
                    'value' => "en-GB-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-GB-Neural2-B",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "en-GB-Neural2-C",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-GB-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "en-GB-Neural2-F",
                    'label' => "Neural2 - FEMALE"
                ]
            ]];
            $voicesData = array_merge($voicesData, $en_GB_nural);
            $en_AU_neural = [
                'en-AU' => [
                    [
                        'value' => "en-AU-Neural2-A",
                        'label' => "Neural2 - FEMALE"
                    ],
                    [
                        'value' => "en-AU-Neural2-B",
                        'label' => "Neural2 - MALE"
                    ],
                    [
                        'value' => "en-AU-Neural2-C",
                        'label' => "Neural2 - FEMALE"
                    ],
                    [
                        'value' => "en-AU-Neural2-D",
                        'label' => "Neural2 - MALE"
                    ],
                ]
            ];
            return $voicesData = array_merge($voicesData, $en_AU_neural);

            $en_US_neural = [
                [
                    'value' => "en-US-Neural2-A",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "en-US-Neural2-C",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-US-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "en-US-Neural2-E",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-US-Neural2-F",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-US-Neural2-G",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-US-Neural2-H",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "en-US-Neural2-I",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "en-US-Neural2-J",
                    'label' => "Neural2 - MALE"
                ],
            ];
            $voicesData = array_merge($voicesData['en-US'], $en_US_neural);

            $fil_ph_neural = [
                [
                    'value' => "fil-ph-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "fil-ph-Neural2-D",
                    'label' => "Neural2 - MALE"
                ]
            ];
            $voicesData = array_merge($voicesData['fil-ph'], $fil_ph_neural);
            $fr_ca_neural = [
                [
                    'value' => "fr-CA-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "fr-CA-Neural2-B",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "fr-CA-Neural2-C",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "fr-CA-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
            ];
            $voicesData = array_merge($voicesData['fr-CA'], $fr_ca_neural);
            $fr_fr_neural = [
                [
                    'value' => "fr-FR-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "fr-FR-Neural2-B",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "fr-FR-Neural2-C",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "fr-FR-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "fr-FR-Neural2-E",
                    'label' => "Neural2 - FEMALE"
                ],
            ];
            $voicesData = array_merge($voicesData['fr-CA'], $fr_fr_neural);
            $de_de_neural = [
                [
                    'value' => "de-DE-Neural2-B",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "de-DE-Neural2-C",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "de-DE-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "de-DE-Neural2-F",
                    'label' => "Neural2 - FEMALE"
                ],
            ];
            $voicesData = array_merge($voicesData['de-DE'], $de_de_neural);
            $hi_in_neural = [
                [
                    'value' => "hi-IN-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "hi-IN-Neural2-B",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "hi-IN-Neural2-C",
                    'label' => "Neural2 - MALE"
                ],
                [
                    'value' => "hi-IN-Neural2-D",
                    'label' => "Neural2 - FEMALE"
                ],
            ];
            $voicesData = array_merge($voicesData['hi-IN'], $hi_in_neural);
            $it_it_neural = [
                [
                    'value' => "it-IT-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "it-IT-Neural2-C",
                    'label' => "Neural2 - MALE"
                ],
            ];
            $voicesData = array_merge($voicesData['it-IT'], $it_it_neural);
            $vi_vn_neural = [
                [
                    'value' => "vi-VN-Neural2-A",
                    'label' => "Neural2 - FEMALE"
                ],
                [
                    'value' => "vi-VN-Neural2-D",
                    'label' => "Neural2 - MALE"
                ],
            ];
            $voicesData = array_merge($voicesData['vi-VN'], $vi_vn_neural);
        }
        return $voicesData;
    }
}
