<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TTSRequestForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required'],
            'project_name' => ['sometimes', 'nullable'],
            'file' => ['required_if:type,google', 'mimes:json']

        ];
    }
}
